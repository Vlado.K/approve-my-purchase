package handlers;

/**
 * //TODO - If needed, validate logic and if possible optimize code.
 */
public class Director extends Approver {


    @Override
    public void approve(Order order) {
        if (canApprove(order)) {
            System.out.println("Director approved purchase with id " + order.getId() + " that costs " + order.getCost());
            return;
        }

        System.out.println("Purchase with id " + order.getId() + " needs approval from higher position than Director.");
        next.approve(order);
    }


    protected boolean canApprove(Order order) {
        return super.canApprove(order, 500
                , 1000, 1500
                , 3500, 6000);

    }
}
