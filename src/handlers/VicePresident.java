package handlers;

/**
 * //TODO - Implement approval implementation for VicePresident level
 */
public class VicePresident extends Approver {
    @Override
    public void approve(Order order) {
        if (canApprove(order)) {
            System.out.println("Vice President approved purchase with id " + order.getId()
                    + " that costs " + order.getCost());
            return;
        }

        System.out.println("Purchase with id " + order.getId() + " needs approval from higher position than Vice President.");
        next.approve(order);
    }

    protected boolean canApprove(Order order) {
        return super.canApprove(order, 700
                , 1500, 2000
                , 4500, 6500);
    }

}
