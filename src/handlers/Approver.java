package handlers;

public abstract class Approver {

    protected Approver next;

    /**
     * If needed, be free to change signature of abstract methods.
     */
    public abstract void approve(Order order);

    protected boolean canApprove(Order order, int canApproveConsumablesOfPrice
            , int canApproveClericalOfPrice, int canApproveGadgetsOfPrice
            , int canApproveGamingOfPrice, int canApprovePcOfPrice) {
        switch (order.getType()) {
            case CONSUMABLES:
                if (order.getCost() <= canApproveConsumablesOfPrice) {
                    return true;
                } else {
                    break;
                }
            case CLERICAL:
                if (order.getCost() <= canApproveClericalOfPrice) {
                    return true;
                } else {
                    break;
                }
            case GADGETS:
                if (order.getCost() <= canApproveGadgetsOfPrice) {
                    return true;
                } else {
                    break;
                }
            case GAMING:
                if (order.getCost() <= canApproveGamingOfPrice) {
                    return true;
                } else {
                    break;
                }
            case PC:
                if (order.getCost() <= canApprovePcOfPrice) {
                    return true;
                } else {
                    break;
                }
            default:
                return false;
        }
        return false;
    }

    /**
     * Method used for registering next approver level.
     * DO NOT CHANGE IT.
     */
    public Approver registerNext(Approver next) {
        this.next = next;
        return next;
    }
}
