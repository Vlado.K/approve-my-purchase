package handlers;

/**
 * //TODO - If needed, validate logic and if possible optimize code
 */
public class Manager extends Approver {
    @Override
    public void approve(Order order) {
        if (canApprove(order)) {
            System.out.println("Manager approved purchase with id " + order.getId() + " that costs " + order.getCost());
            return;
        }

        System.out.println("Purchase with id " + order.getId() + " needs approval from higher position than Manager.");
        next.approve(order);
    }

    protected boolean canApprove(Order order) {
        return super.canApprove(order, 300
                , 500, 1000
                , 3000, 5000);
    }
}
