package handlers;

/**
 * //TODO - Implement approval implementation for President level
 */
public class President extends Approver {
    @Override
    public void approve(Order order) {
        if (canApprove(order)) {
            System.out.println("President approved purchase with id " + order.getId() + " that costs " + order.getCost());
            return;
        }

        System.out.println("Purchase with id " + order.getId() + " needs approval from higher position than President.");
        next.approve(order);
    }


    protected boolean canApprove(Order order) {
        return super.canApprove(order, 1000
                , 2000, 3000
                , 5000, 8000);

    }

}
